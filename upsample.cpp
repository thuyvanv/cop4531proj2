#include <iostream>
// #include <cmath>
// #include <sstream>
//#include <vector>
// #include <fstream>
// #include <cstdlib>
// #include <iterator>
// #include<algorithm>
#include "upsample.h"
using namespace cop4531;
using namespace std;
// vector<int> nearestNeighbor(vector<vector<int> > pixels, int w1, int h1, int w2, int h2);
// vector<int> resizeBilinearGray(vector<vector<int> > pixels, int w, int h, int w2, int h2);
// int l1metric(vector<int> v1, vector<int> v2);

int main(int argc, char *argv[]){
  if(argc != 3) //checks if more than two inputs are put in
  {
    cerr << argv[0] << " [word_file]" << endl;
    exit(EXIT_FAILURE);
  }

  ifstream if1;
  ofstream of1;
  char *inFileString, *outFileString;
  vector<vector<int > > data; //vector with all data in it
  string line;


  int value, value2;
  int i = 0;

  inFileString = argv[1];
  outFileString = argv[2];
  cout << "infilestring " << inFileString << endl;

  if1.open(inFileString); //opens file
    if(!if1){
      cerr << "word file not opened" << endl;
      if1.clear();
    }
    vector<int> tokens;
    //int original[256][256];
    while(getline(if1, line, ',')){ //reads in data and puts in a vector of vectors

      stringstream ss(line);
      int token;
      while(ss >> token){
        tokens.push_back(token);
      //  cout << token << ' ';
        }
          data.push_back(tokens);
        }
      // for(int i = 0; i<data.size(); i++){
      //   for(int j = 0; j<data[i].size(); j++)
      //     cout << data[i][j] << ' ';
      //
      // }  cout << endl;

upsample t0;
vector<int> nearest(1048576);
nearest=t0.nearestNeighbor(data, 256, 256, 512,512); //calls nearest neighbor and puts into variable nearest
of1.open(outFileString); //outputs to another file
  for(int i = 0; i<nearest.size(); i++){
    of1 << nearest[i] << ' ';
  }of1 << endl;
  of1.close();
//int l1;
//l1 = t0.l1metric(tokens,nearest);
//cout << "l1: " << l1 << endl;
return 0;

}
