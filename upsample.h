#ifndef COP4531_UPSAMPLE_H
#define COP4531_UPSAMPLE_H

#include <iostream>
#include <cmath>
#include <sstream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <iterator>
#include<algorithm>

namespace cop4531{

class upsample{
public:
  upsample();
  std::vector<int> nearestNeighbor(std::vector<std::vector<int> > pixels, int w1, int h1, int w2, int h2);
  std::vector<int> resizeBilinearGray(std::vector<std::vector<int> > pixels, int w, int h, int w2, int h2);
  int l1metric(std::vector<int> v1, std::vector<int> v2);
};

#include "upsample.hpp"
}
#endif
