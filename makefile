upsample: upsample.h upsample.hpp upsample.cpp
	g++ -std=c++0x -o upsample upsample.cpp
coverage:
	g++ -std=c++11 -fprofile-arcs -ftest-coverage -fPIC upsample.cpp -o coverage
	./coverage
	./coverage Sample1_input_image.txt
	~/.local/bin/gcovr -r .
test:
	g++ -std=c++11 -I ~/googletest/googletest/include -L ~/googletest/build/lib UnitTest.cpp -o test -lgtest -lpthread
	./test
clean:
	rm *.o upsample
