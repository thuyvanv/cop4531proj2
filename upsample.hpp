upsample::upsample(){

}

std::vector<int> upsample::nearestNeighbor(std::vector<std::vector<int> > pixels, int w1, int h1, int w2, int h2)
{
  int size = w2*h2;
  int newSize = size*4; //finds new size of vector to accommodate for all pixels
  std::vector<int> temp; 
  std::vector<std::vector<int> > newData(newSize);
//  cout << "New size" << newSize << endl;
  double x_ratio = w1/(double)w2;
  double y_ratio = h1/(double) h2;
  int px,py;

  for(int i = 0; i<w1-1; i++){
    for(int j = 0; j<h1-1; j++){
      px = floor(i*x_ratio); //finds pixel place on upscaled image 
      py = floor(j*y_ratio);
      px = (int)px; //makes it an int
      py = (int)py;
      temp.push_back(pixels[px][py]); //puts that value into vector
    }
  }
  return temp;
}

std::vector<int> upsample::resizeBilinearGray(std::vector<std::vector<int> > pixels, int w, int h, int w2, int h2) {
  int size = h2*w2;
  int newSize = size*4;
  std::vector<int>temp;
  float x_ratio = ((float)(w-1))/w2; //find position of new pixel
  float y_ratio = ((float)(h-1))/h2;
  float x_diff, y_diff, ya,yb;
  int gray,x,y, index, aDif, bDif, cDif,dDif;
  int offset = 0;

  for(int i = 0; i<h-1; i++){
    for(int j = 0; j<w-1; j++){
      x = (int)(x_ratio*j);
      y = (int)(y_ratio*i);

      x_diff=(x_ratio*j)-x;
      y_diff = (y_ratio*i)-y;
      index = y*w+x;

      int pointA = pixels[i][j]; //gets four different point
      int pointB = pixels[i+1][j];
      int pointC = pixels[i][j+1];
      int pointD = pixels[i+1][j+1];

       aDif = pointA*(1-x_diff)*(1-y_diff); //calculates difference for each point
       bDif = pointB*(x_diff)*(1-y_diff);
      // bilinear.push_back(bDif);
       cDif = pointC*(y_diff)*(1-x_diff);
       //bilinear.push_back(cDif);
       dDif = pointD*(x_diff*y_diff);
       //bilinear.push_back(dDif);
       gray = aDif+bDif+cDif+dDif;
       int num = pixels[gray][j]; //finds where that pixel is
       temp.push_back(num); //pushes it back into the vector
    }
  } return temp;
}

int upsample::l1metric(std::vector<int> v1, std::vector<int>v2){
int sum = 0;
int num;
std::vector<int>v3;
  for(int i = 0; i<v1.size(); i++){ //subtract num vectors from each other
    num = v1[i]-v2[i];
    num=abs(num); //absolute value of numbers
    v3.push_back(num);
  }
  for(int j = 0; j<v3.size();j++){
    sum += v3[j]; //sums up numbers
  }
  return sum;

}
