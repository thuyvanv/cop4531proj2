# COP4531Proj2
#Image Upsampling Programming Project

I decided to choose the nearest neighbor as the algorithm with the best image scaling. 
To run the program: upsample input.txt output.txt
To run coverage: ./coverage input.txt 
To run test: make test
            ./test
My program might take longer than 3 seconds to run.